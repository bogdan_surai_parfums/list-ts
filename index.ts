class List<T> extends Array {
  public clear() {
    this.splice(0);
  }

  public fromArray(arr: T[]): this {
    this.clear();

    for (const item of arr) {
      this.push(item);
    }

    return this;
  }

  public last(): T | undefined {
    return this[this.length - 1];
  }

  public next(): T {
    const item = this.shift();
    this.push(item);
    return item;
  }

  public shufle(): this {
    let currentIndex = this.length;

    // While there remain elements to shuffle...
    while (currentIndex) {
      // Pick a remaining element...
      currentIndex -= 1;
      const randomIndex = Math.floor(Math.random() * currentIndex);

      // And swap it with the current element.
      const temporaryValue = this[currentIndex];
      const randomValue = this[randomIndex];

      this[currentIndex] = randomValue;
      this[randomIndex] = temporaryValue;
    }

    return this;
  }
}

export default List;
